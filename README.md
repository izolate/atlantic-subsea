# Atlantic Subsea

## Install

```
$ npm install
$ bower install
$ gulp develop|build
$ node --harmony --use-strict app/index.js
```

## Requirements

Node.js, MongoDB, Gulp.js
