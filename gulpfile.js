var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var rupture = require('rupture');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

gulp.task('css', function() {
  return gulp.src('./app/static/styl/index.styl')
    .pipe(plugins.stylus({
      use: [rupture()]
    }))
    .pipe(plugins.autoprefixer())
    .pipe(plugins.csso())
    .pipe(gulp.dest('./app/static/css'));
});

// dev: source maps, debug
gulp.task('js-dev', function () {
  return browserify('./app/static/js/index.js', { debug: true })
    .transform(babelify.configure())
    .bundle()
    .pipe(source('index.js'))
    .pipe(buffer())
    .pipe(plugins.sourcemaps.init({ loadMaps: true }))
    .pipe(gulp.dest('./app/static/dist'));
});

// production js
gulp.task('js-prd', function () {
  return browserify('./app/static/js/index.js', { debug: true })
    .transform(babelify)
    .bundle()
    .pipe(source('index.js'))
    .pipe(buffer())
    .pipe(plugins.uglify())
    .pipe(gulp.dest('./app/static/dist'));
});

gulp.task('watch', function () {
  gulp.watch('./app/static/styl/**/*.styl', ['css']);
  gulp.watch('./app/static/js/**/*.js', ['js-dev']);
});

// development task
gulp.task('develop', ['css', 'js-dev', 'watch']);

// build task
gulp.task('build', ['css', 'js-prd']);
