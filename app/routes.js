let Team = require('./models');

function home(req, res, next) {
  res.render('pages/home', {
    route: 'home'
  });
}

function about(req, res, next) {
  res.render('pages/about', {
    route: 'about'
  });
}

function services(req, res, next) {
  res.render('pages/services', {
    route: 'services'
  });
}

function team(req, res, next) {
  Team.find({}, function(err, team) {
    if (err) next(err);

    res.render('pages/team', {
      route: 'team',
      team: team
    });
  });
}

function addTeam(req, res, next) {
  Team
    .find()
    .sort({ 'sort': 1 })
    .exec(function(err, team) {
      res.render('pages/team-add', {
        team: team
      });
    });
}

function saveTeam(req, res, next) {
  let person = new Team(req.body);

  person.save(function(err,pers) {
    if (err) next(err);
    res.redirect('/team');
  });
}

function contact(req, res, next) {
  res.render('pages/contact', {
    route: 'contact',
  });
}

function commitment(req, res, next) {
  res.render('pages/404');
}

function clients(req, res, next) {
  res.render('pages/clients', {
    route: 'clients'
  });
}

function projects(req, res, next) {
  res.render('pages/projects', {
    route: 'projects'
  });
}

module.exports = function(app) {
  app.get('/', home);
  app.get('/about-us', about);
  app.get('/services', services);

  // team
  app.get('/our-team', team);
  app.get('/team', addTeam);
  app.post('/team', saveTeam);

  app.get('/contact-us', contact);
  app.get('/our-commitment', commitment);
  app.get('/projects', projects);
  app.get('/clients', clients);
};
