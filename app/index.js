let express = require('express');
let config = require('./config');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let app = express();

// views
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

// static files
app.use('/static', express.static(__dirname + '/static'));

// database
let dbUrl = 'mongodb://'+config.mongodb.host+':';
dbUrl += config.mongodb.port+'/'+config.mongodb.collection;

mongoose.connect(dbUrl);

// config
app.use(bodyParser.urlencoded({ extended: false }));

// routes
require('./routes')(app);

app.listen(config.app.port, config.app.ip, function(err) {
  if (err)
    process.exit(10);
  else
    console.log('Atlantic Subsea listening on port ' + config.app.port);
});
