let config = module.exports;
let PRODUCTION = process.env.NODE_ENV === 'production';

config.app = {
  port: process.env.NODE_PORT || 3000,
  ip: '127.0.0.1',
};

config.mongodb = {
  port: process.env.MONGODB_PORT || 27017,
  host: process.env.MONGODB_HOST || 'localhost',
  collection: process.env.MONGODB_COLLECTION || 'atlantic_subsea'
};

if (PRODUCTION) {
  config.app.port = 50505;
}
