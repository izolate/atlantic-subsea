let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let teamSchema = new Schema({
  order: { type: Number, required: true, unique: true },
  name: { type: String, required: true },
  title: String,
  about: String,
  img: String
});

let Team = mongoose.model('teams', teamSchema);

module.exports = Team;
