$('document').ready(() => {
  let slider = $('.homepage__banner');

  if (slider.length)
    slider.unslider({
      dots: false,
      keys: true
    });
});
