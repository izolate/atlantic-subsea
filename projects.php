<?php
	
	$header = array(
		"title" => "Projects - Atlantic Subsea",
		"description" => "Sample projects we've worked on",
		"bodyID" => "projects"
	);
	include "header.php";

?>
	
	<section id="project-list">
		<article>
			<header>
				<h1>Drydock No. 4 Cassion</h1>
				
				<div>
					<h2>Aker Philadelphia Shipyard</h2>
					<span>March 2008</span>
				</div>
			</header>
			
			<div class="images">
				<a href="img/aker-1.jpeg"><img src="img/aker-1t.jpeg"></a>
				<a href="img/aker-2.jpeg"><img src="img/aker-2t.jpeg"></a>
				<a href="img/aker-3.jpeg"><img src="img/aker-3t.jpeg"></a>
			</div>
			
			<p>At the request of Aker Shipyard Philadelphia, Atlantic Subsea constructed a 175 foot long, 60 foot high steel cofferdam wall in 28 days. The cofferdam wall was to temporarily seal the dry-dock and keep it dewatered for the shipbuilding activity. Through design changes, we enabled Aker to successfully continue use of the dry-dock for ship building.</p>
		</article>
		
		<article>
			<header>
				<h1>Autoberth Mooring Dolphin</h1>
				
				<div>
					<h2>Diamond State Port Corp.</h2>
					<span>2009</span>
				</div>
			</header>
			
			<div class="images">
				<a href="img/dspc-1.jpeg"><img src="img/dspc-1t.jpeg"></a>
				<a href="img/dspc-2.jpeg"><img src="img/dspc-2t.jpeg"></a>
				<a href="img/dspc-3.jpeg"><img src="img/dspc-3t.jpeg"></a>
			</div>
			
			<p>In 2009, we were contracted by the DSPC to build a mooring dolphin to accommodate larger ships at the Auto Mooring Berth on the Delaware. The dolphin housed state of the art mooring winches to accommodate larger ships on this berth.</p>
		</article>
		
		<article>
			<header>
				<h1>Lafarge Buchanan FGD Conversion</h1>
				
				<div>
					<h2>PCL Construction Services, Inc</h2>
					<span>August 2009</span>
				</div>
			</header>
			
			<div class="images">
				<a href="img/pcsi-1.jpeg"><img src="img/pcsi-1t.jpeg"></a>
				<a href="img/pcsi-2.jpeg"><img src="img/pcsi-2t.jpeg"></a>
				<a href="img/pcsi-3.jpeg"><img src="img/pcsi-3t.jpeg"></a>
			</div>
			
			<p>In the process of converting Lafarge’s gypsum plant to fly-ash, their marine terminal at Buchanan had to be upgraded to receive bigger ships. In the process of upgrading the marine structures we constructed a crane pier and 3 new mooring dolphins. We also repaired existing structures by underpinned piles. We also transported, installed and assembled a 200 ton crane from Rotterdam on the newly constructed Unloading platform.</p>
		</article>
		
		<article>
			<header>
				<h1>Steel Sheet Bulkhead</h1>
				
				<div>
					<h2>Hess Corporation</h2>
					<span>May 2010</span>
				</div>
			</header>
			
			<div class="images">
				<a href="img/hess-1.jpeg"><img src="img/hess-1t.jpeg"></a>
				<a href="img/hess-2.jpeg"><img src="img/hess-2t.jpeg"></a>
				<a href="img/hess-3.jpeg"><img src="img/hess-3t.jpeg"></a>
			</div>
			
			<p>This was a design build project for Hess Corporation. Due to environmental impacts and restricted timelines we completed the project under budget and ahead of anticipated completion time.</p>
		</article>
		
	</section>
	
	<div class="lightbox">
		<span>Close image</span>
	</div>
	
	<?php include "footer.php"; ?>

</body>
</html>

<!-- Developed by retorq.com -->